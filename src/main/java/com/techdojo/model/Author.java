package com.techdojo.model;

import java.time.LocalDate;

public record Author(int id, String firstName, String lastName, LocalDate dateOfBirth, String country) {
}
