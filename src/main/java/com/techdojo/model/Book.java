package com.techdojo.model;

import java.time.LocalDate;

public record Book(int id, String title, Author author, int pages, int isbn, LocalDate releaseDate, int edition) {
}
