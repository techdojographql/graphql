package com.techdojo.rest;

import com.techdojo.data.DummyData;
import com.techdojo.model.Author;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("rest/authors")
@Tag(name = "authors", description = "Returns authors")
public class AuthorRestController {
    
    @GetMapping
    @Operation(
        description = "return all authors",
        responses = {
            @ApiResponse(
                description = "Returns all authors",
                responseCode = "200"
            ),
            @ApiResponse(
                description = "The request was invalid",
                responseCode = "400",
                content = @Content(schema = @Schema(type = "string", example = "error message"))
            )
        }
    )
    public ResponseEntity<List<Author>> getAll() {
        return ResponseEntity.ok(DummyData.getAllAuthors());
    }
    
    
    @GetMapping("/{id}")
    @Operation(
        description = "return an author by id",
        parameters = {
            @Parameter(
                in = ParameterIn.PATH,
                name = "id",
                schema = @Schema(type = "integer"),
                required = true,
                example = "0",
                description = "The unique identifier of the author")
        },
        responses = {
            @ApiResponse(
                description = "Returns the author with specified id",
                responseCode = "200"
            ),
            @ApiResponse(
                description = "The request was invalid",
                responseCode = "400",
                content = @Content(schema = @Schema(type = "string", example = "error message"))
            )
        }
    )
    public ResponseEntity<Author> get(@NotEmpty @PathVariable int id) {
        return ResponseEntity.ok(DummyData.getAllAuthors()
                                     .get(id));
    }
}