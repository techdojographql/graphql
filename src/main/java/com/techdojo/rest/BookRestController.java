package com.techdojo.rest;

import com.techdojo.data.DummyData;
import com.techdojo.model.Book;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("rest/books")
@Tag(name = "books", description = "Returns books")
public class BookRestController {
    
    @GetMapping
    @Operation(
        description = "return all books",
        responses = {
            @ApiResponse(
                description = "Returns all books",
                responseCode = "200"
            ),
            @ApiResponse(
                description = "The request was invalid",
                responseCode = "400",
                content = @Content(schema = @Schema(type = "string", example = "error message"))
            )
        }
    )
    public ResponseEntity<List<Book>> getAll() {
        return ResponseEntity.ok(DummyData.getAllBooks());
    }
    
    
    @GetMapping("/{id}")
    @Operation(
        description = "return an books by id",
        parameters = {
            @Parameter(
                in = ParameterIn.PATH,
                name = "id",
                schema = @Schema(type = "integer"),
                required = true,
                example = "0",
                description = "The unique identifier of the book")
        },
        responses = {
            @ApiResponse(
                description = "Returns the book with specified id",
                responseCode = "200"
            ),
            @ApiResponse(
                description = "The request was invalid",
                responseCode = "400",
                content = @Content(schema = @Schema(type = "string", example = "error message"))
            )
        }
    )
    public ResponseEntity<Book> get(@NotEmpty @PathVariable int id) {
        return ResponseEntity.ok(DummyData.getAllBooks()
                                     .get(id));
    }
}