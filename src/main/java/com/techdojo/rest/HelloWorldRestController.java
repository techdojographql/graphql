package com.techdojo.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest/helloworld")
@Tag(name = "hello world", description = "Returns hello world!")
public class HelloWorldRestController {
    
    @GetMapping
    @Operation(
        description = "description here",
        responses = {
            @ApiResponse(
                description = "Returns Hello World",
                responseCode = "200"
            ),
            @ApiResponse(
                description = "The request was invalid",
                responseCode = "400",
                content = @Content(schema = @Schema(type = "string", example = "error message"))
            )
        }
    )
    public ResponseEntity<String> get() {
        return ResponseEntity.ok("Hello World");
    }
}
