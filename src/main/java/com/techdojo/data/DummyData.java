package com.techdojo.data;

import com.techdojo.model.Author;
import com.techdojo.model.Book;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.ArrayList;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DummyData {
    static Author REES = new Author(0, "William", "Rees",
                                    LocalDate.of(1980, 10, 5), "Germany");
    static Author PEAKE = new Author(1, "Amelia", "Peake",
                                     LocalDate.of(1975, 8, 3), "France");
    static Author OLIVER = new Author(2, "Jennifer", "Oliver",
                                      LocalDate.of(1990, 11, 5), "United States of America");
    static Author BAILEY = new Author(3, "Ava", "Bailey",
                                      LocalDate.of(1930, 12, 7), "Italy");
    
    public static ArrayList<Author> getAllAuthors() {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(REES);
        authors.add(PEAKE);
        authors.add(OLIVER);
        authors.add(BAILEY);
        return authors;
    }
    
    public static ArrayList<Book> getAllBooks() {
        ArrayList<Book> books = new ArrayList<>();
        
        Book one = new Book(0, "one", REES, 50, 12345678, LocalDate.now(), 1);
        Book two = new Book(1, "two", REES, 50, 12345679, LocalDate.now(), 2);
        Book three = new Book(2, "three", PEAKE, 300, 123456710, LocalDate.now(), 3);
        Book four = new Book(3, "four", OLIVER, 200, 123456711, LocalDate.now(), 3);
        Book five = new Book(4, "five", BAILEY, 50, 123456712, LocalDate.now(), 1);
        books.add(one);
        books.add(two);
        books.add(three);
        books.add(four);
        books.add(five);
        
        return books;
    }
}
